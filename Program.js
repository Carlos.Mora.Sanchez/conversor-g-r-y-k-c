class CalConversor {
  constructor(valor) {
    this.operador1 = valor;
  }

  gradRad() {
    let Rad;
    Rad = this.operador1 * 0.0174;
    return Rad;
  }
  radGrad() {
    let Grad;
    Grad = this.operador1 * 57.2958;
    return Grad;
  }

  kelCen() {
    let Cen;
    Cen = this.operador1 - 273.15;
    return Cen;
  }

  cenKel() {
    let Kel;
    Kel = this.operador1 + 273.15;
    return Kel;
  }
}

let = aresultado = new CalConversor(20);

aresultado.gradRad();
aresultado.radGrad();
aresultado.kelCen();
aresultado.cenKel();
console.log(aresultado.gradRad());
console.log(aresultado.radGrad());
console.log(aresultado.kelCen());
console.log(aresultado.cenKel());